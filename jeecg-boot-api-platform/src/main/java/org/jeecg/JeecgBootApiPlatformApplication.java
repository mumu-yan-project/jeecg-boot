package org.jeecg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import javax.swing.*;

/**
 * @author yanhz
 * @version 1.0
 * @date 2021/10/21 11:30
 */
@SpringBootApplication
@EnableFeignClients
public class JeecgBootApiPlatformApplication {
    public static void main(String[] args) {
        SpringApplication.run(JeecgBootApiPlatformApplication.class, args);
    }
}
