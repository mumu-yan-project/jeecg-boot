package org.jeecg.test.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yanhz
 * @version 1.0
 * @date 2021/10/21 14:38
 */
@Slf4j
@Api(tags = "jeecg-boot-api-platform")
@RestController
@RequestMapping("/hello")
public class HelloController {

    @ApiOperation(value = "测试hello方法", notes = "测试hello方法")
    @GetMapping(value="/")
    public Result<String> hello(){
        Result<String> result = new Result<>();
        result.setResult("Hello World");
        result.setSuccess(true);
        return result;
    }
}
