package org.jeecg.modules.demo.com.test.cesFieldKongj.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.com.test.cesFieldKongj.entity.CesFieldKongj;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: online表单控件
 * @Author: jeecg-boot
 * @Date:   2021-10-20
 * @Version: V1.0
 */
public interface CesFieldKongjMapper extends BaseMapper<CesFieldKongj> {

}
