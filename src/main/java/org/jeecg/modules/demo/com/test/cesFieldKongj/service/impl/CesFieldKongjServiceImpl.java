package org.jeecg.modules.demo.com.test.cesFieldKongj.service.impl;

import org.jeecg.modules.demo.com.test.cesFieldKongj.entity.CesFieldKongj;
import org.jeecg.modules.demo.com.test.cesFieldKongj.mapper.CesFieldKongjMapper;
import org.jeecg.modules.demo.com.test.cesFieldKongj.service.ICesFieldKongjService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: online表单控件
 * @Author: jeecg-boot
 * @Date:   2021-10-20
 * @Version: V1.0
 */
@Service
public class CesFieldKongjServiceImpl extends ServiceImpl<CesFieldKongjMapper, CesFieldKongj> implements ICesFieldKongjService {

}
