package org.jeecg.modules.demo.com.test.cesFieldKongj.service;

import org.jeecg.modules.demo.com.test.cesFieldKongj.entity.CesFieldKongj;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: online表单控件
 * @Author: jeecg-boot
 * @Date:   2021-10-20
 * @Version: V1.0
 */
public interface ICesFieldKongjService extends IService<CesFieldKongj> {

}
